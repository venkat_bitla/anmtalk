import "babel-polyfill";
import 'regenerator-runtime/runtime';
import { AppRegistry, UIManager } from 'react-native';
import './app/push';
import App from './app/index';

UIManager.setLayoutAnimationEnabledExperimental(true);

AppRegistry.registerComponent('anmtalk', () => App);
